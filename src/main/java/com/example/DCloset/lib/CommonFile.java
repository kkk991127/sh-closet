package com.example.DCloset.lib;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public class CommonFile {
    public static File multipartToFile(MultipartFile multipartFile) throws IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir") + "/" + multipartFile.getOriginalFilename());
       // 멀티 파트 파일을 변환해서 저기에 넣으세요. 어디다가 ? convFile에 넣으세요
        multipartFile.transferTo(convFile);
        return convFile;
    }
}
