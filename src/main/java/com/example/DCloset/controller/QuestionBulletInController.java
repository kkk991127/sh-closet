package com.example.DCloset.controller;

import com.example.DCloset.model.questionBulletin.*;
import com.example.DCloset.service.MemberService;
import com.example.DCloset.service.QuestionBulletInService;
import com.example.DCloset.entity.Member;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/questionBulletIn")
public class QuestionBulletInController {
    private final QuestionBulletInService questionBulletInService;
    private final MemberService memberService;

    // 등록 C
    @PostMapping ("/new/member-id/{memberId}")
    public String setQuestionBulletIn(@PathVariable long memberId,@RequestBody QuestionBulletInCreateRequest request){
        Member member = memberService.getMember(memberId);
        questionBulletInService.setQuestionBulletIn(member,request);
        return "ok";
    }

    // 복수 R
    @GetMapping("/list/all")
    public List<QuestionBulletInItem> getQuestionBulletIns()
    {
        return questionBulletInService.getQuestionBulletIns();
    }

    //단수 R
    @GetMapping("/detail/{id}")
    public QuestionBulletInResponse getQuestionBulletIn (@PathVariable long id)
    {return questionBulletInService.getQuestionBulletIn(id);}

    @GetMapping("/detail/member/{memberId}")
    public QuestionBulletInMemberResponse getQuestionMemberBulletIn(@PathVariable long memberId)
    {return questionBulletInService.getQuestionMemberBulletIn(memberId);}


    // id로 수정 하는 U
    @PutMapping("/change/correct-id/{id}")
        public String putQuestionBulletInChangeRequest(@PathVariable long id, @RequestBody QuestionBulletInChangeRequest request){
        questionBulletInService.putQuestionBulletInChangeRequest(id, request);

        return "ok";
    }

    // MemberId로 수정 하는 U
    @PutMapping("/correct/change-member/{memberId}")
    public String putQuestionBulletInChangeMemberRequest(@PathVariable long memberId, @RequestBody QuestionBulletInChangeMemberRequest request){
    questionBulletInService.putQuestionBulletInChangeMemberRequest(memberService.getMember(memberId), request);
        return "ok";
    }



    //id로 답변 상태 수정 할 수 있는
    @PutMapping("/change/status/change-id/{id}")
    public String putQuestionBulletInChangeStatusRequest(@PathVariable long id, @RequestBody QuestionBulletInChangeStatusRequest request){
        questionBulletInService.putQuestionBulletInChangeStatusRequest (id , request);

        return "ok";
    }



    // MemberId로 답변 상태 수정 할 수 있는 U
    @PutMapping("/correct/status/change-member/{memberId}")
    public String putQuestionBulletInChangeMemberStatusRequest(@PathVariable long memberId, @RequestBody QuestionBulletInChangeMemberStatusRequest request){
        questionBulletInService.putQuestionBulletInChangeMemberStatusRequest(memberService.getMember(memberId),request );

        return "ok";
    }


    // 삭제 하는 D
    @DeleteMapping("/delete/{memberId}")
    public String delQuestionBulletIn(@PathVariable long memberId){
        questionBulletInService.delQuestionBulletIn(memberService.getMember(memberId));

        return "ok";
    }




}
