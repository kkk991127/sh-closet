package com.example.DCloset.controller;
import com.example.DCloset.entity.Orders;
import com.example.DCloset.model.delivery.*;
import com.example.DCloset.service.DeliveryService;
import com.example.DCloset.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/delivery")
public class DeliveryController {

    private final DeliveryService deliveryService;
    private final OrderService orderService;


    // 배송 등록
    @PostMapping("/new/orders-id/{ordersId}")
    public String setDelivery(@PathVariable long ordersId, @RequestBody DeliveryCreateRequest deliveryCreateRequest) {

        Orders orders = orderService.getOrders(ordersId);

        deliveryService.setDelivery(orders, deliveryCreateRequest);

        return "ok";
    }
    // 배송 전체 리스트
    @GetMapping("/all")
    public List<DeliveryItem> getDeliveryList() {

        return deliveryService.getDeliveryList();
    }

    // id로 detail 정보 찾기
    @GetMapping("/detail/{id}")
    public DeliveryResponse getDelivery(@PathVariable long id) {
        return deliveryService.getDelivery(id);
    }

    // id로 배송번호, 타입 수정하기
    @PutMapping("/delivery/{id}")
    public String putDeliveryChange(@PathVariable long id, @RequestBody DeliveryChangeRequest request) {

        deliveryService.putDeliveryChange(id, request);

        return "ok";
    }



    //order id로 배송번호 ,타입 수정하기
    @PutMapping("/delivery/order/{ordersId}")
    public String putDeliveryOrderChange(@PathVariable long ordersId, @RequestBody DeliveryChangeOrderRequest request){
        deliveryService.putDeliveryOrderChange(orderService.getOrders(ordersId),request );

        return "ok";
    }





}
