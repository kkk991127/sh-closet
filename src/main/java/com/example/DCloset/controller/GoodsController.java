package com.example.DCloset.controller;


import com.example.DCloset.model.goods.GoodsChangeRequest;
import com.example.DCloset.model.goods.GoodsCreateRequest;
import com.example.DCloset.model.goods.GoodsItem;
import com.example.DCloset.model.goods.GoodsResponse;
import com.example.DCloset.service.GoodsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/goods")
public class GoodsController {

    private final GoodsService goodsService;


    //상품 정보 등록하기
    @PostMapping("/new")
    public String setGoods(@RequestBody GoodsCreateRequest request) {
        goodsService.setGoods(request);
        return "OK";
    }

    //상품 전체 리스트 불러오기
    @GetMapping("/all")
    public List<GoodsItem> getGoodsList (){
        return goodsService.getGoodsList();
    }

    //상품 하나 불러오기
    @GetMapping("/detail/id/{id}")
    public GoodsResponse getGoods(@PathVariable long id) {
        return goodsService.getGoods(id);
    }


    //상품 정보 수정하기
    @PutMapping("/change/id/{id}")
    public String putChange ( @PathVariable long id,@RequestBody GoodsChangeRequest request ){

        goodsService.putChange(id,request);
        return "ok";
    }
}
