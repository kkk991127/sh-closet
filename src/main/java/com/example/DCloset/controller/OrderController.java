package com.example.DCloset.controller;

import com.example.DCloset.model.order.*;
import com.example.DCloset.service.GoodsService;
import com.example.DCloset.service.MemberService;
import com.example.DCloset.entity.Member;
import com.example.DCloset.entity.Goods;
import com.example.DCloset.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/order")
public class OrderController {
    private final OrderService orderService;
    private final MemberService memberService;
    private final GoodsService goodsService;

    // 주문정보 등록하기
    @PostMapping("/new/member-id/{memberId}/goods-id/{goodsId}")
    public String setOrder(@PathVariable long memberId, @PathVariable long goodsId, @RequestBody OrderRequest orderRequest) {
            Member member = memberService.getMember(memberId);
            Goods goods = goodsService.getGood(goodsId);
            orderService.setOrder(member, goods , orderRequest);

        return "OK";
    }

    // 주문 리스트 모두 가져오기
    @GetMapping("/all")
    public List<OrderItem> getOrders() {
        return orderService.getOrders();

    }

    // id로 주문 정보 가져오기 ( 단수 R )
    @GetMapping("/detail/order-id/{id}")
    public OrderResponse getOrder(@PathVariable long id)
    { return orderService.getOrder(id);}

    // member-id 로 주문 정보 조회 ( 단수 R )
    @GetMapping("/detail/member-id/{memberId}")
    public OrderResponse getMember(@PathVariable long memberId){
        return orderService.getMemberOrder(memberId);
    }

    // member-id로 주문 컬러, 사이즈 변경

    @PutMapping("/change/color/size/{memberId}")
    public String putOrderChangeMemberRequest(@PathVariable long memberId, @RequestBody OrderChangeMemberRequest request){
        orderService.putOrderChangeMemberRequest(memberService.getMember(memberId),request );

        return "ok";
    }



    // id로 주문 컬러 , 사이즈 변경
    @PutMapping("/change/color/size/{id}")
    public String putOrderChangeRequest(@PathVariable long id, @RequestBody OrderChangeRequest request){
        orderService.putOrderChangeRequest(id, request);
        return "ok";
    }

    // member id로 주문 상태 수정

    @PutMapping("/change/status/member-id/{memberId}")
    public String putOrderChangeStatusRequest(@PathVariable long memberId, @RequestBody OrderChangeStatusRequest request){
        orderService.putOrderChangeStatusRequest(memberService.getMember(memberId),request);

        return "ok";
    }



    //주문 취소 기능 (삭제  = 주문을 삭제하고 주문 할 수 없는 상품입니다. 라고 보여준다)

    @DeleteMapping("/delete/id/{id}")
    public String delOrder(@PathVariable long id){
        orderService.delOrder(id);
        return "ok";
    }



}
