package com.example.DCloset.service;

import com.example.DCloset.entity.Member;
import com.example.DCloset.entity.QuestionBulletin;
import com.example.DCloset.enums.QuestionStatus;
import com.example.DCloset.model.questionBulletin.*;
import com.example.DCloset.repository.QuestionBulletInRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class QuestionBulletInService {

    private final QuestionBulletInRepository questionBulletInRepository;


    // 게시판 C 등록


    public void setQuestionBulletIn(Member member, QuestionBulletInCreateRequest request){
        QuestionBulletin addData = new QuestionBulletin();
        addData.setMember(member);
        addData.setQuestionCreateDate(request.getQuestionCreateDate());
        addData.setQuestionTitle(request.getQuestionTitle());
        addData.setQuestionContent(request.getQuestionContent());
        addData.setQuestionPassword(request.getQuestionPassword());

        // 기본 값으로 접수로 고정
        addData.setQuestionStatus(QuestionStatus.RECEIVING);

        questionBulletInRepository.save(addData);
    }

    // 게시판 전체 리스트 찾기 (복수 R)
    public List<QuestionBulletInItem> getQuestionBulletIns() {
        List<QuestionBulletin> originList = questionBulletInRepository.findAll();
        List<QuestionBulletInItem> result = new LinkedList<>();

        for(QuestionBulletin questionBulletIn : originList){
            QuestionBulletInItem addItem = new QuestionBulletInItem();
            addItem.setId(questionBulletIn.getId());
            addItem.setMemberId(questionBulletIn.getMember().getId());
            addItem.setQuestionTitle(questionBulletIn.getQuestionTitle());
            addItem.setQuestionContent(questionBulletIn.getQuestionContent());
            addItem.setQuestionPassword(questionBulletIn.getQuestionPassword());
            addItem.setQuestionCreateDate(questionBulletIn.getQuestionCreateDate());
            addItem.setQuestionStatus(questionBulletIn.getQuestionStatus().getQuestionStatus());

            result.add(addItem);
        }
        return result;
    }

    // 단수 R id로 찾기
    public QuestionBulletInResponse getQuestionBulletIn (long id){

        QuestionBulletin originData = questionBulletInRepository.findById(id).orElseThrow();

        QuestionBulletInResponse response = new QuestionBulletInResponse();

        response.setId(originData.getId());
        response.setQuestionTitle(originData.getQuestionTitle());
        response.setQuestionContent(originData.getQuestionContent());
        response.setQuestionPassword(originData.getQuestionPassword());
        response.setQuestionCreateDate(originData.getQuestionCreateDate());
        response.setQuestionStatus(originData.getQuestionStatus().getQuestionStatus());

        return response;
    }

    // 단수 R Member id로 찾기
    public QuestionBulletInMemberResponse getQuestionMemberBulletIn (long memberId){

        QuestionBulletin originData = questionBulletInRepository.findById(memberId).orElseThrow();

        QuestionBulletInMemberResponse response = new QuestionBulletInMemberResponse();

        response.setMemberId(originData.getMember().getId());
        response.setQuestionTitle(originData.getQuestionTitle());
        response.setQuestionPassword(originData.getQuestionPassword());
        response.setQuestionContent(originData.getQuestionContent());
        response.setQuestionCreateDate(originData.getQuestionCreateDate());
        response.setQuestionStatus(originData.getQuestionStatus().getQuestionStatus());


        return response;
    }

    // id로 수정 하는 U
    public void putQuestionBulletInChangeRequest(long id, QuestionBulletInChangeRequest request){
        QuestionBulletin originData = questionBulletInRepository.findById(id).orElseThrow();

        originData.setQuestionTitle(request.getQuestionTitle());
        originData.setQuestionContent(request.getQuestionContent());
        originData.setQuestionPassword(request.getQuestionPassword());

        questionBulletInRepository.save(originData);

    }


    // MemberId로  수정 하는 U
    public void putQuestionBulletInChangeMemberRequest (Member member, QuestionBulletInChangeMemberRequest request){
        QuestionBulletin originData = questionBulletInRepository.findById(member.getId()).orElseThrow();

        originData.setQuestionTitle(request.getQuestionTitle());
        originData.setQuestionContent(request.getQuestionContent());
        originData.setQuestionPassword(request.getQuestionPassword());

        questionBulletInRepository.save(originData);
    }

    // id로 게시판 상태 수정하는 U
    public void putQuestionBulletInChangeStatusRequest (long id , QuestionBulletInChangeStatusRequest request) {
        QuestionBulletin originData = questionBulletInRepository.findById(id).orElseThrow();

        originData.setQuestionStatus(request.getQuestionStatus());

        questionBulletInRepository.save(originData);
    }



    // memberId로 게시판 상태 수정하는 U
    public void putQuestionBulletInChangeMemberStatusRequest (Member member, QuestionBulletInChangeMemberStatusRequest request){
        QuestionBulletin originData = questionBulletInRepository.findById(member.getId()).orElseThrow();

        originData.setQuestionStatus(request.getQuestionStatus());

        questionBulletInRepository.save(originData);
    }



    //삭제 기능 D
    public void delQuestionBulletIn(Member member) {questionBulletInRepository.deleteById(member.getId());}






}
