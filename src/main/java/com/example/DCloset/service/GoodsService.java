package com.example.DCloset.service;

import com.example.DCloset.model.goods.GoodsChangeRequest;
import com.example.DCloset.model.goods.GoodsItem;
import com.example.DCloset.model.goods.GoodsResponse;
import com.example.DCloset.repository.GoodsRepository;
import com.example.DCloset.entity.Goods;
import com.example.DCloset.model.goods.GoodsCreateRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GoodsService {
    private final GoodsRepository goodsRepository;

    public Goods getGood(long id) {return goodsRepository.findById(id).orElseThrow();}



    //상품정보 등록
    public void setGoods(GoodsCreateRequest request) {
        Goods addData = new Goods();
        addData.setProductName(request.getProductName());
        addData.setProductCreateDate(LocalDateTime.now());
        addData.setProductCode(request.getProductCode());
        addData.setProductInfo(request.getProductInfo());
        addData.setGoodsSize(request.getGoodsSize());
        addData.setProductColor(request.getProductColor());
        addData.setProductMainImage(request.getProductMainImage());
        addData.setProductSubImage1(request.getProductSubImage1());
        addData.setProductSubImage2(request.getProductSubImage2());
        addData.setProductSubImage3(request.getProductSubImage3());
        addData.setYnFree(request.getYnFree());
        addData.setYnMembership(request.getYnMembership());
        addData.setYnOffline(request.getYnFree());
        addData.setYnOneDay(request.getYnOneDay());
        addData.setYnPost(request.getYnPost());
        addData.setUseAbleQuantity(request.getUseAbleQuantity());
        addData.setUsingQuantity(request.getUsingQuantity());
        addData.setRepairQuantity(request.getRepairQuantity());

        goodsRepository.save(addData);
    }

    //상품 정보 전체 가져오기
    public List<GoodsItem> getGoodsList(){
        List<Goods> originList = goodsRepository.findAll();
        List<GoodsItem> result = new LinkedList<>();

        for(Goods goods:originList) {
            GoodsItem addItem = new GoodsItem();
            addItem.setId(goods.getId());
            addItem.setProductCreateDate(goods.getProductCreateDate());
            addItem.setProductName(goods.getProductName());
            addItem.setProductCode(goods.getProductCode());
            addItem.setProductInfo(goods.getProductInfo());
            addItem.setGoodsSize(goods.getGoodsSize());
            addItem.setProductColor(goods.getProductColor());

            addItem.setProductMainImage(goods.getProductMainImage());
            addItem.setProductSubImage1(goods.getProductSubImage1());
            addItem.setProductSubImage2(goods.getProductSubImage2());
            addItem.setProductSubImage3(goods.getProductSubImage3());

            addItem.setYnMembership(goods.getYnMembership());
            addItem.setYnOneDay(goods.getYnOneDay());
            addItem.setYnPost(goods.getYnPost());
            addItem.setYnOffline(goods.getYnOffline());
            addItem.setYnFree(goods.getYnFree());

            result.add(addItem);
        }
        return result;
    }

    //상품 단수로 찾
    public GoodsResponse getGoods(long id) {

        Goods originData = goodsRepository.findById(id).orElseThrow();

        GoodsResponse response = new GoodsResponse();

        response.setId(originData.getId());
        response.setProductCreateDate(originData.getProductCreateDate());
        response.setProductName(originData.getProductName());
        response.setProductCode(originData.getProductCode());
        response.setProductInfo(originData.getProductInfo());
        response.setProductColor(originData.getProductColor());
        response.setGoodsSize(originData.getGoodsSize());

        response.setYnMembership(originData.getYnMembership());
        response.setYnOffline(originData.getYnOffline());
        response.setYnFree(originData.getYnFree());
        response.setYnPost(originData.getYnPost());
        response.setYnOneDay(originData.getYnOneDay());

        response.setProductMainImage(originData.getProductMainImage());
        response.setProductSubImage1(originData.getProductSubImage1());
        response.setProductSubImage2(originData.getProductSubImage2());
        response.setProductSubImage3(originData.getProductSubImage3());

        response.setUseAbleQuantity(originData.getUseAbleQuantity());
        response.setUsingQuantity(originData.getUsingQuantity());
        response.setRepairQuantity(originData.getRepairQuantity());

        return response;
    }

    //상품정보 수정 기능
    public void putChange(long id, GoodsChangeRequest request){

        Goods originData = goodsRepository.findById(id).orElseThrow();

        originData.setProductName(request.getProductName());
        originData.setProductCode(request.getProductCode());
        originData.setProductInfo(request.getProductInfo());
        originData.setProductColor(request.getProductColor());
        originData.setGoodsSize(request.getGoodsSize());

        originData.setYnFree(originData.getYnFree());
        originData.setYnPost(originData.getYnPost());
        originData.setYnMembership(originData.getYnMembership());
        originData.setYnOneDay(originData.getYnOneDay());
        originData.setYnOffline(originData.getYnOffline());

        goodsRepository.save(originData);
    }









}
