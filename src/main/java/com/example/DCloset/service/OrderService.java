package com.example.DCloset.service;
import com.example.DCloset.entity.Goods;
import com.example.DCloset.entity.Member;
import com.example.DCloset.model.order.*;
import com.example.DCloset.repository.OrderRepository;
import com.example.DCloset.entity.Orders;
import com.example.DCloset.enums.OrderStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;

    public Orders getOrders(long id) {return orderRepository.findById(id).orElseThrow();}

    //주문 정보 등록

    public void setOrder(Member member, Goods goods, OrderRequest orderRequest) {

        Orders addData = new Orders();
        LocalDate deadLineDate = orderRequest.getDesiredDate().plusDays(10);
        addData.setMember(member);
        addData.setGoods(goods);
        addData.setOrderDate(LocalDate.now());
        addData.setRentalType(orderRequest.getRentalType());
        addData.setGoodsSize(orderRequest.getGoodsSize());
        addData.setGoodsColor(orderRequest.getGoodsColor());
        addData.setDesiredDate(orderRequest.getDesiredDate());
        addData.setDeadlineDate(deadLineDate);

        // 기본값 주문 접수로 고정
        addData.setOrderStatus(OrderStatus.RECEIVE);

        orderRepository.save(addData);

    }


    //주문 정보 전체 조회 복수 R

    public List<OrderItem> getOrders() {
        List<Orders> originList = orderRepository.findAll();
        List<OrderItem> result = new LinkedList<>();

        for(Orders orders : originList) {
            OrderItem addItem = new OrderItem();

            addItem.setId(orders.getId());
            addItem.setMemberId(orders.getMember().getId());
            addItem.setGoodsId(orders.getGoods().getId());
            addItem.setGoodsSize(orders.getGoodsSize());
            addItem.setGoodsColor(orders.getGoodsColor());
            addItem.setOrderDate(orders.getOrderDate());
            addItem.setDesiredDate(orders.getDesiredDate());
            addItem.setDeadlineDate(orders.getDeadlineDate());
            addItem.setOrderStatus(orders.getOrderStatus());

            result.add(addItem);
        }

        return result;
    }

    //주문 정보 주문 ( id 로 조회)

    public OrderResponse getOrder(long id) {
        Orders originData = orderRepository.findById(id).orElseThrow();
        OrderResponse response = new OrderResponse();

        response.setId(originData.getId());
        response.setGoodsId(originData.getGoods().getId());
        response.setMemberId(originData.getMember().getId());
        response.setOrderDate(originData.getOrderDate());
        response.setGoodsColor(originData.getGoodsColor());
        response.setGoodsSize(originData.getGoodsSize());
        response.setDesiredDate(originData.getDesiredDate());
        response.setDeadlineDate(originData.getDeadlineDate());
        response.setOrderStatus(originData.getOrderStatus());

        return response;
    }


    //주문 정보 회원 아이디별 ( member Id 로  조회 ) , 단수 R
    //단수R이 아니라 리스트로 구현해보자.

    public OrderResponse getMemberOrder(long memberId){

        Orders originData = orderRepository.findById(memberId).orElseThrow();
        OrderResponse response = new OrderResponse();

        response.setMemberId(originData.getMember().getId());
        response.setGoodsId(originData.getGoods().getId());
        response.setId(originData.getId());
        response.setGoodsSize(originData.getGoodsSize());
        response.setGoodsColor(originData.getGoodsColor());
        response.setOrderDate(originData.getOrderDate());
        response.setDesiredDate(originData.getDesiredDate());
        response.setDeadlineDate(originData.getDeadlineDate());
        response.setOrderStatus(originData.getOrderStatus());

        return response;
    }

    // member id로 주문 수정하기
    public void putOrderChangeMemberRequest(Member member , OrderChangeMemberRequest request){
        Orders originData = orderRepository.findById(member.getId()).orElseThrow();

        originData.setGoodsSize(request.getGoodsSize());
        originData.setGoodsColor(request.getGoodsColor());

        orderRepository.save(originData);

    }
    // id로 주문 수정하기
    public void putOrderChangeRequest(long id, OrderChangeRequest request){
        Orders originData = orderRepository.findById(id).orElseThrow();

        originData.setGoodsSize(request.getGoodsSize());
        originData.setGoodsColor(request.getGoodsColor());
    }




    //member id로 주문 상태를 변경 할 수 있는 기능

    public void putOrderChangeStatusRequest(Member member, OrderChangeStatusRequest request){
        Orders originData = orderRepository.findById(member.getId()).orElseThrow();

        originData.setOrderStatus(request.getOrderStatus());

        orderRepository.save(originData);
    }





    //주문 취소 기능 (삭제  = 주문을 삭제하고 ,,주문할수 없는 상품입니다. 라고 보여준다)

    public void delOrder(long id){
        orderRepository.deleteById(id);
    }


}
