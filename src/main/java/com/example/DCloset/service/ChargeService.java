package com.example.DCloset.service;
import com.example.DCloset.entity.Charge;
import com.example.DCloset.entity.Member;
import com.example.DCloset.enums.PayType;
import com.example.DCloset.model.charge.ChargeCreateRequest;
import com.example.DCloset.model.charge.ChargeInfoItem;
import com.example.DCloset.model.charge.ChargeChangeRequest;
import com.example.DCloset.model.charge.ChargeResponse;
import com.example.DCloset.repository.ChargeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ChargeService {

    private final ChargeRepository chargeRepository;


    public void setCharge(Member member, ChargeCreateRequest request){

        Charge addData = new Charge();

        addData.setMember(member);
        addData.setActualPayDay(LocalDate.now());
        addData.setPayType(PayType.PAYMENT);
        addData.setFinalAmount(request.getFinalAmount());

        //기본 결제타입은 결제 승인으로 고정
        chargeRepository.save(addData);
    }


    /**
     *
     * @return
     */
    public List<ChargeInfoItem> getChargeList(){

        List<Charge> originList = chargeRepository.findAll();
        List<ChargeInfoItem> result = new LinkedList<>();

        for(Charge charge:originList) {

            ChargeInfoItem addList = new ChargeInfoItem();

            addList.setId(charge.getId());
            addList.setActualPayDay(charge.getActualPayDay());
            addList.setPayType(charge.getPayType());
            addList.setFinalAmount(charge.getFinalAmount());
            addList.setMemberId(charge.getMember().getId());
            result.add(addList);

        }
        return result;
    }

    public ChargeResponse getCharge(long id) {

        Charge originData = chargeRepository.findById(id).orElseThrow();

        ChargeResponse response = new ChargeResponse();

        response.setId(originData.getId());
        response.setActualPayDay(originData.getActualPayDay());
        response.setMemberId(originData.getMember().getId());
        response.setPayType(originData.getPayType());

        response.setFinalAmount(originData.getFinalAmount());


        return response;

    }





    //결제 정보 수정
    public void putChargeChangeRequest(long id, ChargeChangeRequest request){

        Charge originData = chargeRepository.findById(id).orElseThrow();

        originData.setFinalAmount(request.getFinalAmount());

        originData.setPayType(PayType.valueOf(request.getPayType()));

        chargeRepository.save(originData);
        
    }




}
