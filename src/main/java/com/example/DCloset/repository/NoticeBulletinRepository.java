package com.example.DCloset.repository;


import com.example.DCloset.entity.NoticeBulletin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoticeBulletinRepository extends JpaRepository<NoticeBulletin,Long> {
}
