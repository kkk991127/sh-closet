package com.example.DCloset.repository;

import com.example.DCloset.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
    long countByUsername(String username);
}
