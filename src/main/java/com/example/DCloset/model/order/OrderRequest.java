package com.example.DCloset.model.order;

import com.example.DCloset.enums.GoodsSize;
import com.example.DCloset.enums.RentalType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OrderRequest {

    private String goodsColor;
    private GoodsSize goodsSize;
    private RentalType rentalType;
    private LocalDate desiredDate;
}
