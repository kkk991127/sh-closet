package com.example.DCloset.model.order;
import com.example.DCloset.enums.GoodsSize;
import com.example.DCloset.enums.OrderStatus;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class OrderResponse {
    private Long id;
    private Long memberId;
    private Long goodsId;
    private GoodsSize goodsSize;
    private String goodsColor;
    private LocalDate orderDate;
    private LocalDate desiredDate;
    private LocalDate deadlineDate;
    private OrderStatus orderStatus;

}
