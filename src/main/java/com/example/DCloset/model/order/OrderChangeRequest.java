package com.example.DCloset.model.order;


import com.example.DCloset.enums.GoodsSize;

import lombok.Getter;
import lombok.Setter;



@Getter
@Setter
public class OrderChangeRequest {

    private GoodsSize goodsSize;
    private String goodsColor;

}
