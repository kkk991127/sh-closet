package com.example.DCloset.model.member;

import com.example.DCloset.enums.MemberGrade;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberWithdrawal {

    // 회원 탈퇴 시 멤버 등급 -> 비회원으로 변경, 회원 접근 가능한 컨텐츠 접근 시 "로그인이 필요합니다." 안내
    private MemberGrade memberGrade;
}
