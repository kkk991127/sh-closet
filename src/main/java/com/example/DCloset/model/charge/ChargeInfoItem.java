package com.example.DCloset.model.charge;


import com.example.DCloset.enums.PayType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class ChargeInfoItem {


    private Long id;
    private Long memberId;
    private LocalDate actualPayDay;
    private PayType payType;
    private Integer  finalAmount;


}
