package com.example.DCloset.model.charge;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ChargeChangeRequest {


    private String payType;
    private Integer finalAmount;
}
