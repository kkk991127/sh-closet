package com.example.DCloset.model.delivery;


import com.example.DCloset.enums.DeliveryType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;



@Getter
@Setter
public class DeliveryCreateRequest {

    private DeliveryType deliveryType;
    private LocalDate deliveryDate;
    private String deliveryNumber;

}
