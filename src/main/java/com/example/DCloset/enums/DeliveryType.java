package com.example.DCloset.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public enum DeliveryType{

    DELIVERY ("배송"),
    RETURN ("반송");

    private final String deliveryType;

}
