package com.example.DCloset.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RentalType {

    MEMBERSHIP("멤버십 대여"),
    DAILY("하루 대여"),
    OFFLINE("오프라인 대여"),
    EXPERIENCE("무료 대여");

    private final String rentalType;
}
