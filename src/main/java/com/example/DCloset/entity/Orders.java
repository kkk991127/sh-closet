package com.example.DCloset.entity;
import com.example.DCloset.enums.GoodsSize;
import com.example.DCloset.enums.OrderStatus;
import com.example.DCloset.enums.RentalType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberId")
    private Member member;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "goodsId")
    private Goods goods;

    @Column(nullable = false)
    private LocalDate orderDate;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private GoodsSize goodsSize;

    @Column(nullable = false)
    private String goodsColor;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private RentalType rentalType;

    @Column(nullable = false)
    private LocalDate desiredDate;

    @Column(nullable = false)
    private LocalDate deadlineDate;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private OrderStatus orderStatus;



}
