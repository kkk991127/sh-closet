package com.example.DCloset.entity;

import com.example.DCloset.enums.PayType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity

public class Charge {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberId")
    private Member member;

    @Column(nullable = false)
    private LocalDate actualPayDay;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false,length = 20)
    private PayType payType;

    @Column(nullable = false)
    private Integer  finalAmount;

}
