package com.example.DCloset.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity
public class NoticeBulletin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER )
    @JoinColumn(name = "memberId")
    private Member member;

    @Column(nullable = false)
    private LocalDate noticeCreateDate;

    @Column(nullable = false,length = 30)
    private String noticeTitle;

    @Column(columnDefinition = "Text",nullable = false)
    private String noticeContent;



}
